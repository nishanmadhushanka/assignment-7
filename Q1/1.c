#include <stdio.h>
#include <string.h>

int main(){
    char sen[50], rev[50];
    int length, end, i;

    printf("Enter a sentence here: ");
    gets(sen);

    length = strlen(sen);
    end = length - 1;

    for (i = 0; i < length; i++){
        rev[i] = sen[end];
        end--;
    }
    rev[i] = '\0';
    printf("The reverse sentence : %s", rev);

    return 0;
}
