//c program to multiply two given matrixes
#include <stdio.h>

void crt_matrix();   //Function to create matrixes
void matrix(int arr[10][10], int r, int c);  //Function to get elements of the matrix
void mul_matrix();  //Function to multiply two matrixes
void prt_matrix();  //Function to print the answer

int r1, r2, c1, c2;
int arr1[10][10], arr2[10][10];
int mul[10][10];

int main(){
    crt_matrix();
    while (c1 != r2){
        printf("\nError! The two matrixes cannot be multiplied.");
        crt_matrix();
    }
    printf("\nElements of 1st matrix\n");
    matrix(arr1, r1, c1);

    printf("\nElements of 2nd matrix\n");
    matrix(arr2, r2, c2);

    mul_matrix();

    printf("\nMultiplication\n");
    prt_matrix();

    printf("\n");
    return 0;
}

void crt_matrix(){
    printf("Number of rows in 1st matrix:");
    scanf("%d", &r1);
    printf("Number of columns in 1st matrix:");
    scanf("%d", &c1);
    printf("\nNumber of rows in 2nd matrix:");
    scanf("%d", &r2);
    printf("Number of columns in 2nd matrix:");
    scanf("%d", &c2);
}

void matrix(int arr[10][10], int r, int c){
    for (int i = 0; i < r; i++){
        for (int j = 0; j < c; j++){
            printf("Input the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
            }
        }
}

void mul_matrix(){
    for (int i = 0; i < r1; i++){
        for (int j = 0; j < c2; j++){
            for (int k = 0; k < c1; k++){
                mul[i][j] += arr1[i][k] * arr2[k][j];
                }
            }
        }
}

void prt_matrix(){
    for (int i = 0; i < r1; i++){
        for (int j = 0; j < c2; j++){
            printf("%d  ", mul[i][j]);
            if (j == c2 - 1)
                printf("\n");
            }
    }
}


