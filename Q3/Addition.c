//c program to add two given matrixes
#include <stdio.h>

void matrix(int arr[10][10]);  //Function to get elements of the matrix
void add_matrix();  //Function to add two matrixes

int a, b;
int arr1[10][10], arr2[10][10];
int add[10][10];

int main(){
    printf("Number of rows in matrix:");
    scanf("%d", &a);
    printf("Number of columns in matrix:");
    scanf("%d", &b);

    printf("\nElements of 1st matrix\n");
    matrix(arr1);

    printf("\nElements of 2nd matrix\n");
    matrix(arr2);

    printf("\nAddition\n");
    add_matrix();

    printf("\n");
    return 0;
}

void matrix(int arr[10][10]){
    for (int i = 0; i < a; i++){
        for (int j = 0; j < b; j++){
            printf("Input the element of %d,%d: ", i + 1, j + 1);
            scanf("%d", &arr[i][j]);
            }
        }
}

void add_matrix(){
    for (int i = 0; i < a; i++){
        for (int j = 0; j < b; j++){
            add[i][j] = arr1[i][j] + arr2[i][j];
            printf("%d  ", add[i][j]);
            if (j == b - 1)
                printf("\n");
            }
        }
}


