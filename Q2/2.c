#include <stdio.h>

int main(){
    char strg[100], chr;
    int frq = 0;
    int i;

    printf("Enter a string:");
    gets(strg);

    printf("Enter a character:");
    scanf("%c", &chr);

    for(i = 0; strg[i]!='0'; i++){
        if(chr == strg[i])
            frq++;
    }
    printf("Frequency of Character %c = %d", chr, frq);

    return 0;
}
